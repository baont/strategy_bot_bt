import { createChart } from "lightweight-charts";

const chartOptions = {
  layout: {
      textColor: 'black',
      background: { type: 'solid', color: 'white' },
  },
  timeScale: {
                timeVisible: true,
                secondsVisible: false,
            },
};
/** @type {import('lightweight-charts').IChartApi} */
const chart = createChart(document.getElementById('container'), chartOptions);

const series = chart.addCandlestickSeries({
  upColor: '#26a69a', downColor: '#ef5350', borderVisible: false,
  wickUpColor: '#26a69a', wickDownColor: '#ef5350',
});

async function fetchData() {
  try {
    const urlParams = new URLSearchParams(window.location.search);
    const userId = urlParams.get('user_id');
    const alertId = urlParams.get('alert_id');

    const response = await fetch(`http://66.42.48.129:8000/bt-result?user_id=${userId}&alert_id=${alertId}`);
    if (!response.ok) {
      throw new Error('Network response was not ok ' + response.statusText);
    }
    let json = await response.json();
    const data = json["data"]

    // Assuming the API returns an array of objects in the same format as the previous data
    series.setData(data);

    const datesForMarkers = json['alert']

    const markers = [];
    for (let i = 0; i < datesForMarkers.length; i++) {
        markers.push({
          time: datesForMarkers[i],
          position: 'aboveBar',
          color: '#2196F3',
          shape: 'arrowDown'
        });
    }
    series.setMarkers(markers);

    chart.timeScale().fitContent();
  } catch (error) {
    console.error('Error:', error);
  }
}

fetchData();